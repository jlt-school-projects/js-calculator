(function () {
    //LISTENING TO BUTTON CLICKS
    let buttons = document.querySelectorAll('button');
    for (let i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', onButtonClick);
    }

    let historyContainer = document.querySelector('.history');

    //VARIABLE INIALISATION
    let a, b, operator;
    let result = 0;
    let history = [];

    function reset() {
        a = '';
        b = '';
        operator = '';
    }
    reset();

    //KEY DETECTION
    document.addEventListener('keydown',onButtonClick);


    function onButtonClick(e) {
        let value = e.key || e.target.innerText;
        console.log(value);
        if (value.match('[0-9\.]') !== null) {
            handleNumber(value);
        } else if (value === '=' || value ==='Enter') {
            handleEqual();
        } else if (value === 'C' || value === 'Escape' || value === 'E') {
            reset();
            result = 0;
            show(result);
        }
        else {
            handleOperator(value);
        }
    }

    function handleNumber(num) {
        if (operator === '') {
            a += num;
            show(a);
        } else {
            b += num;
            show(b);
        }
    }

    function handleOperator(ope) {
        if (a === '') {
            if (result) {
                a = result.toString();
            } else {
                a = '0';
            }
        }
        operator = ope;
    }


    function handleEqual() {
        if (a === '' && result) {
            a = result.toString();
        }
        if( operator === '' && history.length > 0) {
            operator = history[history.length -1].operator;
        }
        if( b === '' && history.length > 0) {
            b = history[history.length -1].b;
        }
        if (a !== '' && b !== '' && operator !== undefined) {
            //Parsing values;
            let x = parseFloat(a);
            let y = parseFloat(b);
            switch (operator) {
                case '÷':
                case '/':
                    if (y === 0) {
                        show_error('Division par 0');
                        result = 0;
                        break;
                    }
                    result = x / y;
                    break;
                case '×':
                case '*':
                    result = x * y;
                    break;
                case '-':
                    result = x - y;
                    break;
                case '+':
                    result = x + y;
                    break;
                default:
                    show_error('Opérateur inconnu')
            }
            show(result);
            addHistoryEntry({
                a: a,
                operator : operator,
                b: b,
                result: result
            });
            reset();
        } else {
            console.log(a);
            console.log(b);
            console.log(operator);
            console.log(history);
            show_error('Opérande manquante');
        }
    }

    function show(result) {
        let resultdiv = document.querySelector('.result');
        if (resultdiv) {
            resultdiv.innerText = result;
        }
    }

    function show_error(error) {
        alert(error);
        console.log(error);
    }

    function addHistoryEntry(entry){
        history.push(entry);
        let entryElement = document.createElement('div');
        entryElement.textContent = entry.a + entry.operator + entry.b + '=' + entry.result;
        historyContainer.appendChild(entryElement);
    }



})();